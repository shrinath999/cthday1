﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UITesting
{
    public static class BrowserCreation
    {
        public static IWebDriver CreateRemoteDriver(string browserName, string hostName, string port) 
        {
            IWebDriver driver;
            browserName = browserName.ToString().ToUpper();

		    if (hostName == null)
                {
			    hostName="http://localhost"; //assuming that hub is running on localhost
		    }

		    if (port == null) {
			    port = "4444"; //default port of hub
		    }


            //string strURL = hostName + ":" + port + "/wd/hub";

            string strURL = "http://" + hostName + ":" + port + "/wd/hub";

            Trace.WriteLine("hostname " + hostName );
            Trace.WriteLine("port " + port );
            Trace.WriteLine("URL =" + strURL);
		
		    switch (browserName) {

		    case "CHROME":

			    ChromeOptions optionsChrome = new ChromeOptions();
                optionsChrome.AddArguments("test-type");
			    optionsChrome.AddArguments("start-maximized");
			    optionsChrome.AddArguments("--disable-popup-blocking");
			    optionsChrome.AddArguments("disable-extensions");
			    optionsChrome.AddArguments("--disable-notifications");

                    driver = new RemoteWebDriver(new Uri(strURL), optionsChrome);
                    //driver = new RemoteWebDriver(new Uri("http://10.160.202.67:4444"), optionsChrome);

                    break;

		    case "FIREFOX":
			
			    FirefoxOptions optionsff = new FirefoxOptions();
                driver = new RemoteWebDriver(new Uri(strURL), optionsff);
			                break;
		    default:
                    //create chrome by default
                    ChromeOptions optionsChrome1 = new ChromeOptions();
                    optionsChrome1.AddArguments("test-type");
                    optionsChrome1.AddArguments("start-maximized");
                    optionsChrome1.AddArguments("--disable-popup-blocking");
                    optionsChrome1.AddArguments("disable-extensions");
                    optionsChrome1.AddArguments("--disable-notifications");

                    //optionsChrome.setExperimentalOption("useAutomationExtension", false);
                    driver = new RemoteWebDriver(new Uri(strURL), optionsChrome1);
                    break;
		}
 
		return driver;
	} 
    }
}
