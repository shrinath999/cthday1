﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UITesting
{
    public class Challenge3
    {
        IWebDriver driver;
        [SetUp]
        public void SetUp()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
        }

        [Test]
        [Category("Challenge3ProblemA")]
        [Category("Day1")]
        public void ChallengeProblemAValidation()
        {
            //https://cpsatexam.org/
            driver.Navigate().GoToUrl("https://cpsatexam.org/");

            ////li[contains(@class,'slideout-toggle menu-item-align-right')]//a
            By menuItem = By.XPath("//li[contains(@class,'slideout-toggle menu-item-align-right')]//a");
            IWebElement menuElement = driver.FindElement(menuItem);
            menuElement.Click();

            
            driver.FindElement(By.XPath("//a[contains(text(),'Challenge 4')]")).Click();

            //Collectin of speaker

            //Getting the sections first
            By allElementsImages = By.XPath("//div[contains(@class,'elementor-section-wrap')]/section[*]");

            List<IWebElement> sectionList = driver.FindElements(allElementsImages).ToList();

            int sectionSize = sectionList.Count;

            Trace.WriteLine("Number of sections = " + sectionSize);

            for (int i = 0; i < sectionSize; i++)
            {
                //getting speaker for each list by passing i = Row starts from 1, so i+1
                By speakerList = By.XPath("/html[1]/body[1]/div[2]/div[1]/div[1]/main[1]/article[1]/div[1]/div[1]/div[1]/div[1]/div[1]/section[1]/div[1]/div/div[*]");
                List<IWebElement> speakerListInSection = sectionList[i].FindElements(speakerList).ToList();

                int speakersInSectionSize = speakerListInSection.Count;

                string title = driver.Title;

                for (int j = 0; j < speakersInSectionSize; j++)
                {
                    IWebElement weofList = speakerListInSection[j];
                    weofList.Click();

                    var browserTabs = driver.WindowHandles;
                    int number = browserTabs.Count();

                    if (number > 1)
                    {
                        driver.SwitchTo().Window(browserTabs[1]);
                        driver.Close();
                        driver.SwitchTo().Window(browserTabs[0]);
                    }
                }
            }
        }

        [TearDown]
        public void TearDown()
        {
            driver.Quit();
        }



    }
}
