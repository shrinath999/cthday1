﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UITesting
{
    [TestFixture]
    class ParallelExecutionFirefox
    {
        IWebDriver driver;
        [SetUp]
        public void SetUp()
        {
            string host = "10.160.202.67";
            string port = "4444";
            string browserType = "firefox";

            string strURL = "http://" + host + ":" + port + "/wd/hub";

            driver = BrowserCreation.CreateRemoteDriver(browserType, host, port);

        }

        [Test]
        [Category("Grid")]
        public void GridHubNodeFirefox()
        {
            //https://cpsatexam.org/
            driver.Navigate().GoToUrl("https://cpsatexam.org/");

            ////li[contains(@class,'slideout-toggle menu-item-align-right')]//a
            By menuItem = By.XPath("//li[contains(@class,'slideout-toggle menu-item-align-right')]//a");
            IWebElement menuElement = driver.FindElement(menuItem);
            menuElement.Click();

            //Click on challenge 1
            //a[contains(text(),'Challenge 1')]
            driver.FindElement(By.XPath("//a[contains(text(),'Challenge 1')]")).Click();

            //inter
            //i[@class='eicon-close']
            driver.FindElement(By.XPath("//i[@class='eicon-close']")).Click();

            //Taking container div

            By allElementsImages = By.XPath("//div[@id='eael-adv-accordion-cf8d59b']/div[*]");

            List<IWebElement> weList = driver.FindElements(allElementsImages).ToList();

            int size = weList.Count;

            Trace.WriteLine("Size of the list is = " + size);

            string[] paragraphs = new string[size];

            for (int i = 0; i < size; i++)
            {
                IWebElement weofList = weList[i];
                weofList.Click();

                string getLanguageParaHeadingAndPara = weofList.Text;
                string[] splits = getLanguageParaHeadingAndPara.Split(')');
                string header = splits[0] + ")";
                string para = splits[1];
                //Trace to writeline is to write on output
                Trace.WriteLine(header);
                paragraphs[i] = para;



            }

            //Google translate
            for (int i = 0; i < size; i++)
            {
                driver.Navigate().GoToUrl("https://www.google.com/search?q=google+translate&oq=google+trn&aqs=chrome.1.69i57j0l7.4975j0j4&sourceid=chrome&ie=UTF-8");
                driver.FindElement(By.Id("tw-source-text-ta")).SendKeys(Keys.Enter);

                driver.FindElement(By.Id("tw-source-text-ta")).SendKeys(paragraphs[i]);
                //driver.FindElement(By.XPath("//pre[@id='tw-target-text']//span[contains(text(),'Translation')]")).SendKeys(Keys.Enter);
                string translate = driver.FindElement(By.Id("kAz1tf")).Text;

                ////pre[@id='tw-target-text']//span[contains(text(),'Translation')]
                Trace.WriteLine(translate);
            }
        }

        [TearDown]
        public void TearDown()
        {
            driver.Quit();
        }
    }
}
